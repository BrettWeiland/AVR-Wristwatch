#include <stdio.h>
#include <avr/io.h>
#include <util/twi.h>
#include <util/delay.h>

#include "i2c.h"
#include "ssd1306_display_driver.h"
#include "debug.h" //TODO move to timer
#include "br24t_eeprom_driver.h"
#include "pcf_clock_driver.h"
#include "pins.h"
#include "uart.h" 
/**
 * TODO
 * make sure desired functions are static (it's just good practice)
 * crashing, stack tracing, if that ever matters
 * see if stdio is ever needed in non debug builds
 * Battery factors to test:
 *  OLED clock (command 0xd5)
 *  OLED stay on/off
 * organize i2c eeprom/timer code
 */






#ifndef FLASH_EEPROM
int main() {
  LED_DEBUG_DDR |= _BV(LED_DEBUG); //TODO move to debug file or somethin
  LED_DEBUG_PORT &= ~(_BV(LED_DEBUG));
  //initlizes i2c, right now only speed //TODO don't delegate a whole function if desired
  i2c_init();
  screen_init();
#ifdef DEBUG_BUILD

  uart_debug_init();
  FILE stdout_replacement = FDEV_SETUP_STREAM((void *)uart_debug_sendbyte, NULL, _FDEV_SETUP_WRITE);
  FILE stdin_replacement = FDEV_SETUP_STREAM(NULL, (void *)uart_debug_recvbyte, _FDEV_SETUP_READ);
  stdout = &stdout_replacement;
  stdin = &stdin_replacement;

  printf("booted\n");

  screen_lowlvl_testdraw();

  eeprom_testbyte();
  //test_clock();
#endif
  
  
  //initilize timer
  timer_init();

#ifdef FLASH_EEPROM
  //uart_echo();
  flash_eeprom();
#endif


  //initlizes screen registers with good values since we dont have control over reset functionallity
  //screen_init();

  return 0;
}
#endif


