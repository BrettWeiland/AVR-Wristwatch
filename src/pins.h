#ifndef __PINS_INC
#define __PINS_INC
/**
 * CTS is connected to CTS, CTS is 1 when we are ready 
 * scope pins:
 *  1: desktop tx
 *  2: desktop rx
 *  3: desktop rts
 *  4: desktop cts
 */

#include <avr/io.h>

#ifdef __AVR_ATmega328P__
#define UART_DDR    DDRD
#define UART_CTS    PD2 
#define UART_RTS    PD3 
#define UART_PORT   PORTD

#define LED_DEBUG_DDR   DDRB
#define LED_DEBUG_PORT  PORTB
#define LED_DEBUG       PB5 
#elif __AVR_ATmega2560__
#define UART_DDR    DDRB
#define UART_CTS    PB6 
#define UART_RTS    PB5 
#define UART_PORT   PORTB
#define UART_PIN    PINB

#define LED_DEBUG_DDR   DDRB
#define LED_DEBUG_PORT  PORTB
#define LED_DEBUG       PB7
#else
#error "Only have arduino and arduino mega pin mappings!"
#endif



#endif

/**
yelow = slave rts/master cts
cyan = slave cts/master rts
pink = slave tx/master tx
blue = slave rx/slave tx
**/
