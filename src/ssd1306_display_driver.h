#ifndef __SSD1306_H
#define __SSD1306_H

#include <stdint.h>

#define SCREEN_RES_X        128
#define SCREEN_RES_Y        32

uint8_t screen_buffer[(SCREEN_RES_X * SCREEN_RES_Y) / 8];

void screen_init();
void screen_testdraw();
void screen_off();
void screen_on();
void screen_update();

#endif
