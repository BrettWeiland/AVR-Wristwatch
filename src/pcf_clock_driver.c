#include <util/delay.h>
#include <stdio.h>
#include "i2c.h"
#define CLOCK_ADDR 0x51

#ifdef FLASH_EEPROM
void flash_clock() { 
  i2c_write_reg(EEPROM_ADDR, 0x00, 0x58); //resets clock
  i2c_write_reg(CLOCK_ADDR, 0x00, 0x40); //sets to 12 hour time
  i2c_write_reg(CLOCK_ADDR, 0x01, 0x08); //sets alarm interrupts on
}
#endif
