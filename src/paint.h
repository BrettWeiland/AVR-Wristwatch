#ifndef __PAINT_H
#define __PAINT_H

#include "ssd1306_display_driver.h"
#include "br24t_eeprom_driver.h"

typedef const struct font {
  EEPROM_ADDR *font; 
  uint8_t width;
  uint8_t height;
  uint16_t first_char;
} font_t;

void screen_clear();
void draw_image(EEPROM_ADDR image);
void draw_hline(int pos_y);
void draw_text(char *text, int x, int y, font_t font);


#endif

