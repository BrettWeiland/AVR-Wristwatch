#ifndef __UART_INC
#define __UART_INC
void uart_debug_init();
void uart_debug_sendbyte(uint8_t byte);
uint8_t uart_debug_recvbyte();

void uart_flash_init();
void uart_flash_sendbyte(uint8_t byte);
uint8_t uart_flash_recvbyte();

#define HARDWARE_FLOW 1 //TODO
#endif
