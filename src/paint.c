#include <string.h>
#include "ssd1306_display_driver.h"
#include "paint.h"
#include "br24t_eeprom_driver.h"

#define TWOD_INDEX(x, y) ((y * SCREEN_RES_Y) / 8) + x //TODO rename to insinuate /8
                                                      //
/** I'm not using the screen's buffer as a standard 2d array, 
 *  this is because of how pixels are represented in the display's GDRAM.
 *
 *  While it would be nicer to just resort the array every time it's updated,
 *  keeping driver abstractions in it's own file (like we should),
 *  this way we don't have to resort every time we redraw.
 *
 *  Some operations may not need a resort, for example, images/videos optmimized
 *  for GDRAM. Doing it in each function helps us prevent unnessesary calculations
 *  while doing real time operations like videos.
 */

void screen_clear() { memset(&screen_buffer, 0, sizeof(screen_buffer)); }

//images are optimized to follow page formatting
void draw_image(EEPROM_ADDR image) {
  for(int on_pix = 0; on_pix < (SCREEN_RES_X * SCREEN_RES_Y) / 8; on_pix++)
    screen_buffer[on_pix] = EEPROM_READBYTE(image + on_pix); 
}

//however here we need to compensate 
void draw_hline(int pos_y) {
  for(int on_pix = 0; on_pix < SCREEN_RES_X; on_pix++)
    screen_buffer[((pos_y / 8) * 8) + on_pix] |= (1 << (pos_y % 8));
}

//also here
void draw_text(char *text, int x, int y, font_t font) { 
  for(int c = 0; text[c] == 0; c++) {
  }
}
