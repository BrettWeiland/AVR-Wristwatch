#ifndef __DEBUG_SERIAL_H
#define __DEBUG_SERIAL_H

#include <stdio.h>
#include <avr/io.h>
#include "uart.h"
#include "pins.h"

void timer_init();
void test_timer();
void scroll_test();
void screen_lowlvl_testdraw();
void uart_echo();
void test_clock();
void eeprom_testbyte();
void debug_crash();
unsigned int ticks_to_seconds();

#define DEBUG_LED_ON() LED_DEBUG_PORT |= _BV(LED_DEBUG)
#define DEBUG_LED_OFF() LED_DEBUG_PORT &= ~(_BV(LED_DEBUG))



#endif
