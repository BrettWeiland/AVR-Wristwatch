#!/usr/bin/env python3
import serial
import argparse
import os
from time import sleep
from math import ceil

#we send syn, recv ack
SYN_MAGIC = 0xdeadbeef 
ACK_MAGIC = 0xf00dd00d

EEPROM_PAGESIZE = 64
EEPROM_FILE = "./compiled_eeprom"


parser = argparse.ArgumentParser(description="sends binary file over serial so the avr can write it to eeprom")
parser.add_argument('port', type=str)
parser.add_argument('baud', type=int)
args = parser.parse_args()

eeprom_file = open("./compiled_eeprom", "rb")
eeprom_filesize = os.fstat(eeprom_file.fileno()).st_size
page_len = ceil(eeprom_filesize / EEPROM_PAGESIZE)

s = serial.Serial(port=args.port, baudrate=args.baud, parity=serial.PARITY_NONE, timeout=0, rtscts=0, dsrdtr=0, write_timeout = 0)
s.rts = 0

#slow method
def write_hwf(data):
    #check if the other guy is already trying to send
    debugval = 0
    print("writing")
    data_parsed = [data[b:b+1] for b in range(len(data))]
    for c in data_parsed:
        print(debugval)
        if s.cts:
            print("Slave is trying to send before we do!")
           exit(1)
        debugval += 1
        s.rts = 1 
        while not s.cts:
            pass
        s.write(c)
        while s.cts:
            pass
        s.rts = 0
        print("done writing")

def read_hwf(length):
    print("reading")
    msg = b''
    for c in range(length):
        print("waiting for CTS")
        while not s.cts:
            pass
        s.rts = 1
        msg += s.read(size=1)
        s.rts = 0
        print("waiting for CTS to go back up, recieved {}".format(msg))
        while s.cts:
            pass
        print("done reading")
    return(msg)

#def handle_writer_error(write_status):

#while True:
#    write_hwf(b"abcdef\r\n")
#    print(read_hwf(10))

input()
blah = 0
tots_blah = 0
while True:
    tots_blah += 1
    print("PACKET {}".format(tots_blah))
    if blah > 8:
        blah = 0
    blah += 1
    write_hwf(bytes("Debugstr {}\r\n".format(blah), 'UTF-8'))
    print(read_hwf(12))

print(page_len)
write_hwf(page_len.to_bytes(1, 'little'))


for page in range(0, page_len):
    print("PAGE: {}".format(page))
    if EEPROM_PAGESIZE > (eeprom_filesize - eeprom_file.tell()):
        print("writing and padding last page")
        write_hwf(eeprom_file.read())
        [write_hwf(b'\x00') for b in range(0, eeprom_filesize % EEPROM_PAGESIZE)]
    else:
        write_hwf(eeprom_file.read(EEPROM_PAGESIZE))
    write_status = int(read_hwf(1))
    print("WRITE STATUS: {}".format(write_status))
    if write_status == 0:
        pass
    elif write_status == 1: # verification failed
        print("Verification failed on page {}!".format(page))
    elif write_status == 0xff:
        print("Unknown error writing page {}!".format(page))

print("Done writing to EEPROM!\n")
