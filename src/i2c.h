#ifndef __I2C_H
#define __I2C_H

#include <stddef.h>
#include <stdbool.h>

#define I2C_WRITE   0
#define I2C_READ    1

//higher level functions write registers
//TODO rename if we end up only using for screen
void i2c_write_reg(uint8_t device_addr, uint8_t device_reg, uint8_t value);
void i2c_write_reg_multi(uint8_t device_addr, uint8_t device_reg, size_t len, uint8_t *values);

//for drivers with weird i2c processes
void i2c_send(uint8_t byte);
uint8_t i2c_recv();
void i2c_stop();
void i2c_start(uint8_t addr, bool rw);
uint8_t i2c_read_reg_addr16(uint8_t device, uint16_t addr);
void i2c_write_reg_addr16(uint8_t device_addr, uint16_t device_reg, uint8_t data);

void i2c_init();


#endif
