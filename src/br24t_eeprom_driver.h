#ifndef __BR24_INC
#define __BR24_INC

#include <stdint.h>
#include "i2c.h"

#define EEPROM_I2C_ADDR   0x50


#ifdef FLASH_EEPROM
void flash_eeprom();
#endif

typedef uint16_t EEPROM_ADDR;

#define EEPROM_READBYTE(addr) i2c_read_reg_addr16(EEPROM_I2C_ADDR, addr)
#define EEPROM_WRITEBYTE(addr, data) i2c_write_reg_addr16(EEPROM_I2C_ADDR, addr, data);


#endif


