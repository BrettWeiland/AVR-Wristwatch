#!/usr/bin/env python3

#
(int(xy / 16) * 16) + int((((xy - (int(xy / 16) * 16)))) / 8) + ((xy - ((int(xy / 16) * 16) + int(((xy - (int(xy / 16) * 16))) / 8) * 8)) * 2)

BLEN = 2
WIDTH = 8
HEIGHT = 4

for xy in range(0, 32):
    page_offset = int(xy / (BLEN * WIDTH)) * (BLEN * WIDTH)
    collumn_offset = int((xy - page_offset) / WIDTH)

    bit_offset = page_offset + collumn_offset + ((xy - (page_offset + collumn_offset * WIDTH)) * BLEN)

    page_location = bit_offset
    print(page_location)
    #print("pixel {}: bit {}".format(xy, column_offset))

