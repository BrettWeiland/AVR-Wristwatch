# AVR WRISTWATCH
## What is it?
This repository contains the code in development (and soon schematics) for a small wristwatch with a 128x32 OLED display. I'm hoping it will last the whole day, have cool animated visuals, and be pretty to look at.

## Desired Features and Goals 
* 128x32 OLED monochrome display
* 128kb upgradable external EEPROM for images and short videos
* Fancy graphics, display images and videos that can overlay time with inverting filter
* Battery that lasts at least a day
* USB-C charging (may switch to micro-usb depending on IC prices/availability)
* Display the time

## Parts
* Microcontroller: Any ATmega chip with TWI/I2C I can get my hands on
* Display: Waveshare 128x32 0.91inch OLED display 
* RTC clock: PCF85063A
* EEPROM: BR24T128-W
* Battery Management: TBD
  

## Why
This project is to help me learn how to develop for AVR without Arduino libraries, read datasheets faster, and to prove I can solder small SMD components. It will be my first physical project where I design my own PCB, which has been something I've wanted to do for a long time.

